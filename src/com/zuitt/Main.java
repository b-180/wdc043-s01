package com.zuitt;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        String firstName, lastName;
        Double firstSubject = 0.00, secondSubject = 0.00, thirdSubject = 0.00, ave =0.00;



        Scanner scan = new Scanner(System.in).useDelimiter("\\n");

        System.out.println("Enter your first name:");
        firstName = scan.nextLine();

        System.out.println("Enter your last name:");
        lastName = scan.nextLine();

        System.out.println("Enter first subject:");
        firstSubject = scan.nextDouble();

        System.out.println("Enter second subject:");
        secondSubject = scan.nextDouble();


        System.out.println("Enter third subject:");
        thirdSubject = scan.nextDouble();

        ave = ((firstSubject + secondSubject + thirdSubject) / 3);

        System.out.println("Good day, " + lastName + " " + firstName + ".");
        System.out.format("Your grade average is: %.2f", ave);
    }
}

//public static double findAverage()